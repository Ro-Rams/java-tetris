### What is this repository for? ###

Tetris game implemented using Java and it's Swing library.
All basic shapes are added and a simple score system.

### Scope for improvement ###

1) Have shadow of Tetromino appear at bottom of board.
2) Show controls
3) Add animation of Row clearing
4) Add border
5) Show next Tetromino up
6) Save Tetromino
7) Improve art of Tetromino by making them 3D