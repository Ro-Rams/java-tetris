package board;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import board.model.Coordinate;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import tetromino.TetrominoBlock;

public class BoardControllerTest {

	private BoardController board;
	
	@Before
	public void setup() {
		BoardInvariants xInvariants = new BoardInvariants(11, 5, 15);
		BoardInvariants yInvariants = new BoardInvariants(27, 0, 15);
		board = new BoardController(xInvariants, yInvariants);
	}
	
	@Test
	public void testCanMoveLeft() {
		TetrominoBlock position = new TetrominoBlock(5, 0, true);
		
		assertTrue(board.canMoveLeft(position));
	}
	
	@Test
	public void testAtLeftEdge() {
		TetrominoBlock position = new TetrominoBlock(0, 0, true);
		
		assertFalse(board.canMoveLeft(position));
	}
	
	@Ignore // Can't getCurrentTetromino the mock to work correctly
	@Test
	public void testLeftSpaceOccupied() {
		TetrominoBlock position = new TetrominoBlock(1, 0, true);
		Coordinate coord = mock(Coordinate.class);
		doReturn(true).when(coord).isOccupied();
		when(coord.isOccupied()).thenReturn(true);
		
		assertFalse(board.canMoveLeft(position));
	}
	
	@Test
	public void testCanMoveRight() {
		TetrominoBlock position = new TetrominoBlock(5, 0, true);
		
		assertTrue(board.canMoveRight(position));
	}
	
	@Test
	public void testAtRightEdge() {
		TetrominoBlock position = new TetrominoBlock(10, 0, true);
		
		assertFalse(board.canMoveRight(position));
	}
	
	@Ignore // Can't getCurrentTetromino the mock to work correctly
	@Test
	public void testRightSpaceOccupied() {
		TetrominoBlock position = new TetrominoBlock(1, 0, true);
		Coordinate coord = mock(Coordinate.class);
		doReturn(true).when(coord).isOccupied();
		when(coord.isOccupied()).thenReturn(true);
		
		assertFalse(board.canMoveRight(position));
	}
	
	@Test
	public void testCanMoveDown() {
		TetrominoBlock position = new TetrominoBlock(5, 0, true);
		
		assertTrue(board.canMoveDown(position));
	}
	
	@Test
	public void testAtBottom() {
		TetrominoBlock position = new TetrominoBlock(0, 26, true);
		
		assertFalse(board.canMoveDown(position));
	}
	
	@Ignore // Can't getCurrentTetromino the mock to work correctly
	@Test
	public void testBelowSpaceIsOccupied() {
		TetrominoBlock position = new TetrominoBlock(1, 0, true);
		Coordinate coord = mock(Coordinate.class);
		doReturn(true).when(coord).isOccupied();
		when(coord.isOccupied()).thenReturn(true);
		
		assertFalse(board.canMoveDown(position));
	}

}
