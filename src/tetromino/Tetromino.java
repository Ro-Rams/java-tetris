package tetromino;

import board.BoardController;
import tetromino.adjust.Adjust;
import tetromino.exception.TetrominoException;
import tetromino.rotation.values.Values;

import java.awt.*;

public class Tetromino {

    private final Color colour;
    private final Adjust adjust;
    private final Values values;
    private TetrominoBlock[] positions;

    public Tetromino(TetrominoSettings settings) {
        this.colour = settings.getColour();
        this.positions = settings.getPositions();
        this.adjust = settings.getAdjust();
        this.values = settings.getValues();
    }

    public void setStartPosition(TetrominoBlock startPosition) {
        setupStaticBlock(startPosition);
        adjust.initalAdjust(this);
        adjust.adjust(this);
    }

    private void setupStaticBlock(TetrominoBlock startPosition) {
        for (TetrominoBlock position : positions) {
            if (position.isStaticBlock()) {
                position.setColumn(startPosition.getColumn());
                position.setRow(startPosition.getRow());
            }
        }
    }

    public TetrominoBlock[] getTetrominoPositions() {
        return positions;
    }

    public Color getColour() {
        return colour;
    }

    public TetrominoBlock getStaticBlock() {
        for (TetrominoBlock position : positions) {
            if (position.isStaticBlock()) {
                return position;
            }
        }
        throw new TetrominoException("Static block not found on tetromino " + this);
    }

    public void rotate(BoardController board) {
        if (values != null) {
            TetrominoBlock staticBlock = getStaticBlock();
            TetrominoBlock[] positions = getTetrominoPositions();
            if (board.canRotate(staticBlock, values)) {
                applyRotation(positions, staticBlock, values);
                values.changeOrintation();
            }
        }
    }

    private void applyRotation(TetrominoBlock[] positions, TetrominoBlock staticBlock, Values values) {
        int count = 0;
        for (TetrominoBlock position : positions) {
            if (!position.isStaticBlock()) {
                position.setColumn(staticBlock.getColumn() + values.getXValues()[count]);
                position.setRow(staticBlock.getRow() + values.getYValues()[count]);
                count++;
            }
        }
    }

    public void resetOrintation() {
        if (values != null) {
            values.resetOrintation();
        }
    }


}
