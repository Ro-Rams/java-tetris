package tetromino;

import board.BoardController;

public class Actions {

	public void moveLeft(Tetromino tetromino, BoardController board) {
		if (canMoveLeft(tetromino, board))
			decrementX(tetromino.getTetrominoPositions());
	}
	
	private boolean canMoveLeft(Tetromino tetromino, BoardController board) {
		for (TetrominoBlock position : tetromino.getTetrominoPositions()) {
			if (!board.canMoveLeft(position)) {
				return false;
			}
		}
		return true;
	}
	
	private void decrementX(TetrominoBlock[] positions) {
		for (TetrominoBlock position : positions) {
			position.decrementColumn();
		}
	}
	
	public void moveRight(Tetromino tetromino, BoardController board) {
		if (canMoveRight(tetromino, board))
			incrementX(tetromino.getTetrominoPositions());
	}
	
	private boolean canMoveRight(Tetromino tetromino, BoardController board) {
		for (TetrominoBlock position : tetromino.getTetrominoPositions()) {
			if (!board.canMoveRight(position)) {
				return false;
			}
		}
		return true;
	}
	
	private void incrementX(TetrominoBlock[] positions) {
		for (TetrominoBlock position : positions) {
			position.incrementColumn();
		}
	}
	
	public boolean moveDown(Tetromino tetromino, BoardController board) {
		if (canMoveDown(tetromino, board)) {
			incrementY(tetromino.getTetrominoPositions());
			return false;
		} else {
			return true;
		}
	}
	
	private boolean canMoveDown(Tetromino tetromino, BoardController board) {
		for (TetrominoBlock position : tetromino.getTetrominoPositions()) {
			if (!board.canMoveDown(position)) {
				return false;
			}
		}
		return true;
	}
	
	private void incrementY(TetrominoBlock[] positions) {
		for (TetrominoBlock position : positions) {
			position.incrementRow();
		}
	}
	
	public void jumpToBottom(Tetromino tetromino, BoardController board) {
		board.occupyLowestBlocks(tetromino.getTetrominoPositions(), tetromino.getColour());
	}
	
	public void resetOrintation(Tetromino tetromino) {
		tetromino.resetOrintation();
	}
}