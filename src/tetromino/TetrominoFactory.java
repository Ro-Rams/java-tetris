package tetromino;

import tetromino.adjust.*;
import tetromino.rotation.values.*;

import java.awt.*;
import java.util.Random;

public class TetrominoFactory {

    private final Random rand;
    private final TetrominoSettings SQUARE;
    private final TetrominoSettings LINE;
    private final TetrominoSettings L;
    private final TetrominoSettings J;
    private final TetrominoSettings T;
    private final TetrominoSettings S;
    private final TetrominoSettings Z;

    public TetrominoFactory() {
        rand = new Random();
        SQUARE = new TetrominoSettings(new TetrominoBlock[] { generateInitialStaticBlock(), generateInitialBlock(), generateInitialBlock(), generateInitialBlock() }, Color.YELLOW, new SquareAdjust(), null);
        LINE = new TetrominoSettings(new TetrominoBlock[] { generateInitialStaticBlock(), generateInitialBlock(), generateInitialBlock(), generateInitialBlock() }, Color.CYAN, new LineAdjust(), new LineValues());
        L = new TetrominoSettings(new TetrominoBlock[] { generateInitialStaticBlock(), generateInitialBlock(), generateInitialBlock(), generateInitialBlock() }, Color.BLUE, new LAdjust(), new LValues());
        J = new TetrominoSettings(new TetrominoBlock[] { generateInitialStaticBlock(), generateInitialBlock(), generateInitialBlock(), generateInitialBlock() }, Color.ORANGE, new JAdjust(), new JValues());
        T = new TetrominoSettings(new TetrominoBlock[] { generateInitialStaticBlock(), generateInitialBlock(), generateInitialBlock(), generateInitialBlock() }, Color.MAGENTA, new TAdjust(), new TValues());
        S = new TetrominoSettings(new TetrominoBlock[] { generateInitialStaticBlock(), generateInitialBlock(), generateInitialBlock(), generateInitialBlock() }, Color.GREEN, new SAdjust(), new SValues());
        Z = new TetrominoSettings(new TetrominoBlock[] { generateInitialStaticBlock(), generateInitialBlock(), generateInitialBlock(), generateInitialBlock() }, Color.RED, new ZAdjust(), new ZValues());
    }

    private TetrominoBlock generateInitialStaticBlock() {
        return new TetrominoBlock(0, 0, true);
    }

    private TetrominoBlock generateInitialBlock() {
        return new TetrominoBlock(0, 0, false);
    }

    public Tetromino getRandomTetromino() {
        int n = rand.nextInt(70)+1;
        if (n < 10) {
            return new Tetromino(SQUARE);
        } else if (n < 20) {
            return new Tetromino(LINE);
        } else if (n < 30) {
            return new Tetromino(L);
        } else if (n < 40) {
            return new Tetromino(J);
        } else if (n < 50) {
            return new Tetromino(T);
        } else if (n < 60) {
            return new Tetromino(S);
        } else {
            return new Tetromino(Z);
        }
    }
}
