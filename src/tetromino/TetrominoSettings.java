package tetromino;

import tetromino.adjust.Adjust;
import tetromino.rotation.values.Values;

import java.awt.*;

/**
 * Created by roryarmstrong on 18/03/2016.
 */
public class TetrominoSettings {

    private final TetrominoBlock[] positions;
    private final Color colour;
    private final Adjust adjust;
    private final Values values;

    public TetrominoSettings(TetrominoBlock[] positions, Color colour, Adjust adjust, Values values) {
        this.positions = positions;
        this.colour = colour;
        this.adjust = adjust;
        this.values = values;
    }

    public TetrominoBlock[] getPositions() {
        TetrominoBlock[] copyPositions = new TetrominoBlock[4];
        for (int x = 0; x < positions.length; x++) {
            copyPositions[x] = new TetrominoBlock(positions[x].getColumn(), positions[x].getRow(), positions[x].isStaticBlock());
        }
        return copyPositions;
    }

    public Color getColour() {
        return new Color(colour.getRGB());
    }

    public Adjust getAdjust() {
        return adjust;
    }

    public Values getValues() {
        return values;
    }
}
