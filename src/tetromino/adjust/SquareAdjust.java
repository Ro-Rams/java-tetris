package tetromino.adjust;

import tetromino.Tetromino;
import tetromino.TetrominoBlock;

public class SquareAdjust extends Adjust {

	public void adjust(Tetromino tetromino) {
		int count = 0;
		for (TetrominoBlock position : tetromino.getTetrominoPositions()) {
			if (!position.isStaticBlock()) {
				applySquarePosition(count, position);
				count++;
			}
		}
	}
	
	private void applySquarePosition(int count, TetrominoBlock position) {
		switch (count) {
		case 0:	position.incrementRow();
				position.incrementColumn();
				break;
		case 1: position.incrementRow();
				break;
		case 2: position.incrementColumn();
		default:
				break;
		}
	}
}
