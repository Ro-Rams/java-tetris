package tetromino.adjust;

import tetromino.Tetromino;
import tetromino.TetrominoBlock;

public class LAdjust extends Adjust {

	public void adjust(Tetromino tetromino) {
		int count = 0;
		for (TetrominoBlock position : tetromino.getTetrominoPositions()) {
			if (!position.isStaticBlock()) {
				applyLPosition(count, position);
				count++;
			} else {
				position.incrementRow();
			}
		}
	}
	
	private void applyLPosition(int count, TetrominoBlock position) {
		switch(count) {
			case 0: position.incrementColumn();
					break;
			case 1: position.decrementColumn();
					position.incrementRow();
					break;
			case 2: position.incrementColumn();
					position.incrementRow();
					break;
		}
	}
}
