package tetromino.adjust;

import tetromino.Tetromino;
import tetromino.TetrominoBlock;

public class ZAdjust extends Adjust {

	public void adjust(Tetromino tetromino) {
		int count = 0;
		for (TetrominoBlock position : tetromino.getTetrominoPositions()) {
			if (!position.isStaticBlock()) {
				applyJPosition(count, position);
				count++;
			} else {
				position.incrementRow();
			}
		}
	}
	
	private void applyJPosition(int count, TetrominoBlock position) {
		switch(count) {
			case 0: position.decrementColumn();
					break;
			case 2: position.incrementColumn();
					position.incrementRow();
					break;
		}
	}
}
