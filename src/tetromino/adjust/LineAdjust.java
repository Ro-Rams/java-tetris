package tetromino.adjust;

import tetromino.Tetromino;
import tetromino.TetrominoBlock;

public class LineAdjust extends Adjust {

	public void adjust(Tetromino tetromino) {
		int count = 0;
		for (TetrominoBlock position : tetromino.getTetrominoPositions()) {
			if (!position.isStaticBlock()) {
				applyLinePosition(count, position);
				count++;
			}
		}
	}
	
	private void applyLinePosition(int count, TetrominoBlock position) {
		switch(count) {
			case 0: position.decrementColumn();
					break;
			case 1: position.incrementColumn();
					break;
			case 2: position.incrementColumn();
					position.incrementColumn();
					break;
			default:
					break;
		}
	}
}
