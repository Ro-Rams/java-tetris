package tetromino.adjust;

import tetromino.Tetromino;
import tetromino.TetrominoBlock;

public class SAdjust extends Adjust {

	public void adjust(Tetromino tetromino) {
		int count = 0;
		for (TetrominoBlock position : tetromino.getTetrominoPositions()) {
			if (!position.isStaticBlock()) {
				applyJPosition(count, position);
				count++;
			} else {
				position.incrementRow();
			}
		}
	}
	
	private void applyJPosition(int count, TetrominoBlock position) {
		switch(count) {
			case 0: position.decrementColumn();
					position.incrementRow();
					break;
			case 2: position.incrementColumn();
					break;
		}
	}
}
