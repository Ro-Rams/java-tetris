package tetromino.adjust;

import tetromino.Tetromino;
import tetromino.TetrominoBlock;

public abstract class Adjust {

	public abstract void adjust(Tetromino tetromino);

	public void initalAdjust(Tetromino tetromino) {
		TetrominoBlock staticBlock = tetromino.getStaticBlock();
		for (TetrominoBlock position : tetromino.getTetrominoPositions()) {
			position.setColumn(staticBlock.getColumn());
			position.setRow(staticBlock.getRow());
		}
	}
}
