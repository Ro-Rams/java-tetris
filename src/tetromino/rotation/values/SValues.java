package tetromino.rotation.values;


public class SValues implements Values {

	private final int[] xOrintationOne;
	private final int[] xOrintationTwo;
	private final int[] yOrintationOne;
	private final int[] yOrintationTwo;
	private final int length;
	private int orintation;

	public SValues() {
		xOrintationOne = new int[] { -1, 0, 1 };
		xOrintationTwo = new int[] { 0, 1, 1 };
		yOrintationOne = new int[] { 0, -1, -1 };
		yOrintationTwo = new int[] { -1, 0, 1 };
		length = xOrintationOne.length;
		orintation = 0;
	}
	
	public int[] getXValues() {
		switch(orintation) {
		case 0:
			return xOrintationTwo;
		default:
			return xOrintationOne;
		}
	}
	
	public int[] getYValues() {
		switch(orintation) {
		case 0:
			return yOrintationTwo;
		default:
			return yOrintationOne;
		}
	}
	
	public int length() {
		return length;
	}
	
	public void changeOrintation() {
		orintation = (orintation >= 1) ? 0 : orintation + 1;
	}
	
	public void resetOrintation() {
		orintation = 0;
	}
}
