package tetromino.rotation.values;


public class LineValues implements Values {

	private final int[] orintationOne;
	private final int[] orintationTwo;
	private final int length;
	private int orintation;
	
	public LineValues() {
		orintationOne = new int[] { -1, 1, 2 };
		orintationTwo = new int[] { 0, 0, 0 };
		length = orintationOne.length;
		orintation = 0;
	}
	
	public int[] getXValues() {
		switch(orintation) {
		case 0:
			return orintationTwo;
		default:
			return orintationOne;
		}
	}
	
	public int[] getYValues() {
		switch(orintation) {
		case 0:
			return orintationOne;
		default:
			return orintationTwo;
		}
	}
	
	public int length() {
		return length;
	}
	
	public void changeOrintation() {
		orintation = (orintation >= 1) ? 0 : orintation + 1;
	}
	
	public void resetOrintation() {
		orintation = 0;
	}
}
