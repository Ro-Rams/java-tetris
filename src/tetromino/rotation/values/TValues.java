package tetromino.rotation.values;


public class TValues implements Values {

	private final int[] xOrintationOne;
	private final int[] xOrintationTwo;
	private final int[] xOrintationThree;
	private final int[] xOrintationFour;
	private final int[] yOrintationOne;
	private final int[] yOrintationTwo;
	private final int[] yOrintationThree;
	private final int[] yOrintationFour;
	private final int length;
	private int orintation;

	public TValues() {
		xOrintationOne = new int[] { 0, -1, 1 };
		xOrintationTwo = new int[] { -1, 0 , 0 };
		xOrintationThree = new int[]  { 0, 1, -1 };
		xOrintationFour = new int[]  { 1, 0, 0 };
		yOrintationOne = new int[] { 1, 0, 0 };
		yOrintationTwo = new int[] { 0, -1, 1 };
		yOrintationThree = new int[]  { -1, 0, 0 };
		yOrintationFour = new int[]  { 0, 1, -1 };
		length = xOrintationOne.length;
		orintation = 0;
	}
	
	public int[] getXValues() {
		switch(orintation) {
		case 0:
			return xOrintationTwo;
		case 1:
			return xOrintationThree;
		case 2:
			return xOrintationFour;
		default:
			return xOrintationOne;
		}
	}
	
	public int[] getYValues() {
		switch(orintation) {
		case 0:
			return yOrintationTwo;
		case 1:
			return yOrintationThree;
		case 2:
			return yOrintationFour;
		default:
			return yOrintationOne;
		}
	}
	
	public int length() {
		return length;
	}
	
	public void changeOrintation() {
		orintation = (orintation >= 3) ? 0 : orintation + 1;
	}
	
	public void resetOrintation() {
		orintation = 0;
	}
}
