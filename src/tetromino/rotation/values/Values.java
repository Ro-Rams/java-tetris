package tetromino.rotation.values;

public interface Values {

	public int[] getXValues();
	public int[] getYValues();
	public int length();
	public void changeOrintation();
	public void resetOrintation();
}
