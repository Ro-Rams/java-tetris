package tetromino;

public class TetrominoBlock {
	
	private int column;
	private int row;
	private boolean staticBlock;
	
	public TetrominoBlock(int column, int row, boolean staticBlock) {
		this.column = column;
		this.row = row;
		this.staticBlock = staticBlock;
	}
	
	public int getColumn() {
		return column;
	}
	
	public int getRow() {
		return row;
	}
	
	public void decrementRow() {
		row -= 1;
	}
	
	public void incrementRow() {
		row += 1;
	}
	
	public void decrementColumn() {
		column -= 1;
	}
	
	public void incrementColumn() {
		column += 1;
	}

	public void setColumn(int column) {
		this.column = column;
	}
	
	public void setRow(int row) {
		this.row = row;
	}
	
	public boolean isStaticBlock() {
		return staticBlock;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (staticBlock ? 1231 : 1237);
		result = prime * result + column;
		result = prime * result + row;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null || getClass() != obj.getClass())
			return false;
		TetrominoBlock other = (TetrominoBlock) obj;
		if (staticBlock != other.staticBlock)
			return false;
		if (column != other.column)
			return false;
		if (row != other.row)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TetrominoPosition [column=" + column + ", row=" + row + ", staticBlock="
				+ staticBlock + "]";
	}
}
