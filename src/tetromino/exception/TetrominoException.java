package tetromino.exception;

public class TetrominoException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public TetrominoException() {
		super();
	}
	
	public TetrominoException(String message) {
		super(message);
	}
}
