package board;

import board.model.Board;
import board.model.Coordinate;
import tetromino.TetrominoBlock;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public class BoardTransformer {

    private Board board;

    public BoardTransformer(Board board) {
        this.board = board;
    }

    public int vacatRow(int score) {
        int rowNum = -1;
        for (Integer key : board.keySet()) {
            int rowCount = 0;
            for (Coordinate coord : board.get(key)) {
                if (!coord.isOccupied()) {
                    break;
                }
                rowCount++;
            }
            if (rowCount == board.columnSize()) {
                rowNum = key;
                break;
            }
        }
        if (rowNum >= 0) {
            moveAllOccupiedCoordsDown(rowNum);
            score += 100;
            return vacatRow(score);
        }
        return score;
    }

    private void moveAllOccupiedCoordsDown(int rowToRemove) {
        Map<Integer, Coordinate> previousRowsOccupiedCoords = getUnoccupiedRow();
        for (Integer key : board.keySet()) {
            if (key != rowToRemove) {
                Map<Integer, Coordinate> occupiedCoords = getOccupiedCoords(board.get(key));
                applyNewOccupiedValues(board.get(key), previousRowsOccupiedCoords);
                previousRowsOccupiedCoords = occupiedCoords;
            } else {
                applyNewOccupiedValues(board.get(key), previousRowsOccupiedCoords);
                break;
            }
        }
    }

    private Map<Integer, Coordinate> getUnoccupiedRow() {
        Map<Integer, Coordinate> unoccupiedRow = new HashMap<>();
        for (int x = 0; x < board.columnSize(); x++) {
            unoccupiedRow.put(x, new Coordinate(0, 0, false));
        }
        return unoccupiedRow;
    }

    private Map<Integer, Coordinate> getOccupiedCoords(Coordinate[] row) {
        Map<Integer, Coordinate> occupiedCoords = new HashMap<>();
        for (int x = 0; x < row.length; x++) {
            Coordinate coord = row[x];
            occupiedCoords.put(x, new Coordinate(coord.getColumnCoordinate(), coord.getRowCoordinate(), coord.isOccupied(), coord.getColour()));
        }
        return occupiedCoords;
    }

    private void applyNewOccupiedValues(Coordinate[] row, Map<Integer, Coordinate> previousRowsOccupiedCoords) {
        for (int x = 0; x < row.length; x++) {
            Coordinate previousCoord = previousRowsOccupiedCoords.get(x);
            if (previousCoord.isOccupied()) {
                row[x].occupy(previousCoord.getColour());
            } else {
                row[x].vacat();
            }
        }
    }

    public boolean completeRow() {
        for (Integer key : board.keySet()) {
            int rowCount = 0;
            for (Coordinate coord : board.get(key)) {
                if (!coord.isOccupied()) {
                    break;
                }
                rowCount++;
            }
            if (rowCount == board.columnSize()) {
                return true;
            }
        }
        return false;
    }

    public void clearAll() {
        for (Integer key : board.keySet()) {
            for (Coordinate coord : board.get(key)) {
                coord.vacat();
            }
        }
    }

    public void occupyLowestBlocks(TetrominoBlock[] positions, Color tetrominoColour) {
        TetrominoBlock highestBlockFrom = null;
        TetrominoBlock highestBlockTo = null;
        for (TetrominoBlock position : positions) {
            TetrominoBlock copyPosition = new TetrominoBlock(position.getColumn(), position.getRow(), position.isStaticBlock());
            getLowestPosition(copyPosition);
            if (highestBlockFrom == null) {
                highestBlockFrom = position;
                highestBlockTo = copyPosition;
            } else if (!validBottomPosition(positions, highestBlockFrom, highestBlockTo)) {
                highestBlockFrom = position;
                highestBlockTo = copyPosition;
            } else if (betterPosition(highestBlockFrom, highestBlockTo, position, copyPosition)) {
                highestBlockFrom = position;
                highestBlockTo = copyPosition;
            }
        }
        occupyBlocks(positions, highestBlockFrom, highestBlockTo, tetrominoColour);
    }

    private boolean validBottomPosition(TetrominoBlock[] positions, TetrominoBlock highestBlockFrom, TetrominoBlock highestBlockTo) {
        for (TetrominoBlock position : positions) {
            int rowValue = getPositionDifference(position.getRow(), highestBlockFrom.getRow(), highestBlockTo.getRow());
            int columnValue = getPositionDifference(position.getColumn(), highestBlockFrom.getColumn(), highestBlockTo.getColumn());
            if (!board.validRow(rowValue)) {
                return false;
            } else if (!board.validColumn(columnValue)) {
                return false;
            } else if (board.columnHasOccupiedBlock(rowValue, columnValue)) {
                return false;
            }
        }
        return true;
    }

    private boolean betterPosition(TetrominoBlock highestBlockFrom, TetrominoBlock highestBlockTo, TetrominoBlock position, TetrominoBlock copyPosition) {
        return betterToPosition(highestBlockTo, copyPosition) && betterFromPosition(highestBlockFrom, position);
    }

    private boolean betterToPosition(TetrominoBlock highestBlockTo, TetrominoBlock copyPosition) {
        return copyPosition.getRow() <= highestBlockTo.getRow();
    }

    private boolean betterFromPosition(TetrominoBlock highestBlockFrom, TetrominoBlock position) {
        return position.getRow() >= highestBlockFrom.getRow();
    }

    private void getLowestPosition(TetrominoBlock position) {
        if (board.positionIsOccupied(position.getRow(), position.getColumn())) {
            position.decrementRow();
            getLowestPosition(position);
        } else if (position.getRow()+1 < board.rowSize() && !board.positionIsOccupied(position.getRow()+1, position.getColumn())) {
            position.incrementRow();
            getLowestPosition(position);
        }
    }

    private void occupyBlocks(TetrominoBlock[] positions, TetrominoBlock highestBlockFrom, TetrominoBlock highestBlockTo, Color tetrominoColour) {
        for (TetrominoBlock position : positions) {
            int yValue = getPositionDifference(position.getRow(), highestBlockFrom.getRow(), highestBlockTo.getRow());
            int xValue = getPositionDifference(position.getColumn(), highestBlockFrom.getColumn(), highestBlockTo.getColumn());
            if (yValue >= 0) {
                Coordinate coord = board.getCoordinateInBoard(yValue, xValue);
                coord.occupy(tetrominoColour);
            }
        }
    }

    private int getPositionDifference(int current, int winner, int winnerBottomValue) {
        if (current > winner) {
            return winnerBottomValue + (current - winner);
        } else if (winner > current) {
            return winnerBottomValue - (winner - current);
        } else {
            return winnerBottomValue;
        }
    }
}
