package board;

import java.awt.Color;
import java.awt.Graphics;

import board.model.Board;
import board.model.BoardFactory;
import tetromino.rotation.values.Values;
import tetromino.TetrominoBlock;
import view.GameDrawer;

public class BoardController {

	private final Board immutableBoard;
	private final BoardTransformer transformer;
	private final GameDrawer drawer;
	private final BoardValidation validator;

	public BoardController(BoardInvariants columnInvariants, BoardInvariants rowInvariants) {
		BoardFactory factory = new BoardFactory(columnInvariants, rowInvariants);
		immutableBoard = factory.createImmutableBoard();
		Board mutableBoard = factory.createMutableBoard();
		transformer = new BoardTransformer(mutableBoard);
		drawer = new GameDrawer(immutableBoard);
		validator = new BoardValidation(immutableBoard);
	}

	public TetrominoBlock getStartPosition() {
		return immutableBoard.getStartPosition();
	}

	public boolean canMoveLeft(TetrominoBlock position) {
		return validator.canMoveLeft(position);
	}
	
	public boolean canMoveRight(TetrominoBlock position) {
		return validator.canMoveRight(position);
	}
	
	public boolean canMoveDown(TetrominoBlock position) {
		return validator.canMoveDown(position);
	}

	public boolean canRotate(TetrominoBlock staticBlock, Values values) {
		return validator.canRotate(staticBlock, values);
	}
	
	public void occupyLowestBlocks(TetrominoBlock[] positions, Color tetrominoColour) {
		transformer.occupyLowestBlocks(positions, tetrominoColour);
	}
	
	public boolean gameEnded() {
		return immutableBoard.gameEnded();
	}
	
	public void drawTetromino(TetrominoBlock[] positions, Graphics g, Color colour) {
		drawer.drawTetromino(positions, g, colour);
	}

	public void drawNextTetromino(TetrominoBlock[] positions, Graphics g, Color colour) {
		drawer.drawNextTetromino(positions, g, colour);
	}
	
	public void draw(Graphics g) {
		drawer.drawUnoccupiedBlocks(g);
		drawer.drawOccupiedBlocks(g);
	}
	
	public int vacatRow(int score) {
		return transformer.vacatRow(score);
	}
	
	public boolean completeRow() {
		return transformer.completeRow();
	}

	public void clearAll() {
		transformer.clearAll();
	}

	@Override
	public String toString() {
		return "BoardController{" +
				"immutableBoard=" + immutableBoard +
				", transformer=" + transformer +
				", drawer=" + drawer +
				", validator=" + validator +
				'}';
	}
}