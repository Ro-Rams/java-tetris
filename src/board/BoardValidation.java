package board;

import board.model.Board;
import tetromino.rotation.values.Values;
import tetromino.TetrominoBlock;

public class BoardValidation {

    private Board board;

    public BoardValidation(Board board) {
        this.board = board;
    }

    public boolean canMoveLeft(TetrominoBlock position) {
        return !atLeftEdgeOfBoard(position.getColumn()) && !leftSpaceOccupied(position);
    }

    private boolean atLeftEdgeOfBoard(int x) {
        return x <= 0;
    }

    private boolean leftSpaceOccupied(TetrominoBlock position) {
        return board.positionIsOccupied(position.getRow(), position.getColumn()-1);
    }

    public boolean canMoveRight(TetrominoBlock position) {
        return !atRightEdgeOfBoard(position.getColumn()) && !rightSpaceOccupied(position);
    }

    private boolean rightSpaceOccupied(TetrominoBlock position) {
        return board.positionIsOccupied(position.getRow(), position.getColumn()+1);
    }

    private boolean atRightEdgeOfBoard(int x) {
        return x >= board.columnSize()-1;
    }

    public boolean canMoveDown(TetrominoBlock position) {
        if (atBottom(position.getRow())) {
            return false;
        }
        return !board.positionIsOccupied(position.getRow()+1, position.getColumn());
    }

    private boolean atBottom(int y) {
        return y >= board.rowSize()-1;
    }

    public boolean canRotate(TetrominoBlock staticBlock, Values values) {
        for (int m = 0; m < values.length(); m++) {
            int column = staticBlock.getColumn() + values.getXValues()[m];
            int row = staticBlock.getRow() + values.getYValues()[m];
            if (!board.validColumn(column) || !board.validRow(row) || board.columnHasOccupiedBlock(row, column)) {
                return false;
            }
        }
        return true;
    }
}
