package board;

public class BoardInvariants {

	private final int positionsSize;
	private final int initialPosition;
	private final int moveDistance;
	
	public BoardInvariants(int positionsSize, int initialPosition, int moveDistance) {
		this.positionsSize = positionsSize;
		this.initialPosition = initialPosition;
		this.moveDistance = moveDistance;
	}
	
	public int getPositionsSize() {
		return positionsSize;
	}
	
	public int getInitialPosition() {
		return initialPosition;
	}
	
	public int getMoveDistance() {
		return moveDistance;
	}

	public boolean validPosition(int position) {
		if (position >= 0 && position < positionsSize) {
			return true;
		}
		return false;
	}

	public int closestBoundary(int value) {
		return (positionsSize/2 > value) ? positionsSize-1 : 0;
	}

	@Override
	public String toString() {
		return "BoardInvariants [positionsSize=" + positionsSize
				+ ", initialPosition=" + initialPosition + ", moveDistance="
				+ moveDistance + "]";
	}
	
}
