package board.model;

import java.awt.Color;

public class Coordinate {

	private static final Color DEFAULT_COLOR = Color.GRAY;
	private int columnCoordinate;
	private int rowCoordinate;
	private boolean occupied;
	private Color colour;
	
	public Coordinate(int columnCoordinate, int rowCoordinate, boolean occupied) {
		this(columnCoordinate, rowCoordinate, occupied, DEFAULT_COLOR);
	}

	public Coordinate(int columnCoordinate, int rowCoordinate, boolean occupied, Color colour) {
		this.columnCoordinate = columnCoordinate;
		this.rowCoordinate = rowCoordinate;
		this.occupied = occupied;
		this.colour = colour;
	}
	
	public int getColumnCoordinate() {
		return columnCoordinate;
	}
	
	public int getRowCoordinate() {
		return rowCoordinate;
	}
	
	public void occupy(Color colour) {
		occupied = true;
		this.colour = colour;
	}
	
	public void vacat() {
		occupied = false;
		colour = DEFAULT_COLOR;
	}
	
	public boolean isOccupied() {
		return occupied;
	}
	
	public Color getColour() {
		return colour;
	}

	public Coordinate copy() {
		return new Coordinate(columnCoordinate, rowCoordinate, occupied, colour);
	}

	@Override
	public String toString() {
		return "Coordinates [columnCoordinate=" + columnCoordinate + ", rowCoordinate="
				+ rowCoordinate + ", occupied=" + occupied + "]";
	}
}
