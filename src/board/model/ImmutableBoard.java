package board.model;

import board.BoardInvariants;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by roryarmstrong on 13/03/2016.
 */
public class ImmutableBoard extends Board {

    protected ImmutableBoard(Map<Integer, Coordinate[]> board, BoardInvariants columnInvariants, BoardInvariants rowInvariants) {
        super(board, columnInvariants, rowInvariants);
    }

    @Override
    public Coordinate[] get(Integer key) {
        Coordinate[] row = super.get(key);
        Coordinate[] copiedRow = new Coordinate[row.length];
        for (int x = 0; x < row.length; x++) {
            copiedRow[x] = row[x].copy();
        }
        return copiedRow;
    }

    @Override
    public Set<Integer> keySet() {
        Set<Integer> keySet = board.keySet();
        Set<Integer> copySet = new HashSet<>();
        for (Integer key : keySet) {
            copySet.add(new Integer(key));
        }
        return copySet;
    }

    @Override
    public Coordinate getCoordinateInBoard(int row, int column) {
        return super.getCoordinateInBoard(row, column).copy();
    }
}
