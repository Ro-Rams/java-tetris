package board.model;

import board.BoardInvariants;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class BoardFactory {

    private final Map<Integer, Coordinate[]> board;
    private final BoardInvariants columnInvariants;
    private final BoardInvariants rowInvariants;

    public BoardFactory(BoardInvariants columnInvariants, BoardInvariants rowInvariants) {
        this.columnInvariants = columnInvariants;
        this.rowInvariants = rowInvariants;
        board = createBoard();
    }

    private Map<Integer, Coordinate[]> createBoard() {
        Map<Integer, Coordinate[]> board = new ConcurrentHashMap<>();
        for (int y = 0; y < rowInvariants.getPositionsSize(); y++) {
            Coordinate[] row = new Coordinate[columnInvariants.getPositionsSize()];
            for (int x = 0; x < columnInvariants.getPositionsSize(); x++) {
                row[x] = new Coordinate(columnInvariants.getMoveDistance() * (x+1), rowInvariants.getMoveDistance() * (y+1), false);
            }
            board.put(y, row);
        }
        return board;
    }

    public ImmutableBoard createImmutableBoard() {
        return new ImmutableBoard(board, columnInvariants, rowInvariants);
    }

    public Board createMutableBoard() {
        return new MutableBoard(board, columnInvariants, rowInvariants);
    }
}
