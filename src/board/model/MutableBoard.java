package board.model;

import board.BoardInvariants;

import java.util.Map;

/**
 * Created by roryarmstrong on 13/03/2016.
 */
public class MutableBoard extends Board {

    public MutableBoard(Map<Integer, Coordinate[]> board, BoardInvariants columnInvariants, BoardInvariants rowInvariants) {
        super(board, columnInvariants, rowInvariants);
    }
}
