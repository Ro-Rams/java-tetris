package board.model;

import board.BoardInvariants;
import tetromino.TetrominoBlock;

import java.util.Map;
import java.util.Set;

/**
 * Created by roryarmstrong on 13/03/2016.
 */
public abstract class Board {

    protected Map<Integer, Coordinate[]> board;
    protected final BoardInvariants columnInvariants;
    protected final BoardInvariants rowInvariants;

    public Board(Map<Integer, Coordinate[]> board, BoardInvariants columnInvariants, BoardInvariants rowInvariants) {
        this.board = board;
        this.columnInvariants = columnInvariants;
        this.rowInvariants = rowInvariants;
    }

    public TetrominoBlock getStartPosition() {
        return new TetrominoBlock(columnInvariants.getInitialPosition(), rowInvariants.getInitialPosition(), true);
    }

    public boolean gameEnded() {
        if (board.get(0)[columnInvariants.getInitialPosition()].isOccupied())
            return true;
        return false;
    }

    public int rowSize() {
        return rowInvariants.getPositionsSize();
    }

    public int columnSize() {
        return columnInvariants.getPositionsSize();
    }

    public boolean validRow(int rowValue) {
        return rowInvariants.validPosition(rowValue);
    }

    public boolean validColumn(int columnValue) {
        return columnInvariants.validPosition(columnValue);
    }

    public int rowMoveDistance() {
        return rowInvariants.getMoveDistance();
    }

    public int columnMoveDistance() {
        return columnInvariants.getMoveDistance();
    }

    public Set<Integer> keySet() {
        return board.keySet();
    }

    public Coordinate[] get(Integer key) {
        return board.get(key);
    }

    public boolean columnHasOccupiedBlock(int rowValue, final int columnValue) {
        for (int y = rowValue; y >= 0; y--) {
            if (getCoordinateInBoard(y, columnValue).isOccupied()) {
                return true;
            }
        }
        return false;
    }

    public boolean positionIsOccupied(int row, int column) {
        return getCoordinateInBoard(row, column).isOccupied();
    }

    public Coordinate getCoordinateInBoard(int row, int column) {
        return board.get(rowOutOfBoundsCheck(row))[columnOutOfBoundsCheck(column)];
    }

    private int rowOutOfBoundsCheck(int row) {
        if (!rowInvariants.validPosition(row)) {
            return rowInvariants.closestBoundary(row);
        }
        return row;
    }

    private int columnOutOfBoundsCheck(int column) {
        if (!columnInvariants.validPosition(column)) {
            return columnInvariants.closestBoundary(column);
        }
        return column;
    }

    @Override
    public String toString() {
        return "Board{" +
                "board=" + board +
                ", columnInvariants=" + columnInvariants +
                ", rowInvariants=" + rowInvariants +
                '}';
    }
}
