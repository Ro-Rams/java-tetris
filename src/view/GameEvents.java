package view;

import board.BoardController;
import tetromino.Actions;
import view.model.GameTetromino;
import view.model.Score;

import javax.swing.*;
import java.awt.event.KeyEvent;

public class GameEvents {

    private final JPanel gamePanel;
    private final Actions actions;
    private final Score score;
    private final GameTetromino currentTetromino;
    private final BoardController board;
    private final Timer timer;
    private boolean pause;
    private boolean gameEnded;
    private boolean rotationPainted;
    private boolean gameStarted;

    public GameEvents(JPanel gamePanel, Score score, BoardController board, Timer timer, GameTetromino currentTetromino) {
        rotationPainted = true;
        pause = true;
        gameEnded = false;
        gameStarted = false;
        this.gamePanel = gamePanel;
        actions = new Actions();
        this.score = score;
        this.board = board;
        this.currentTetromino = currentTetromino;
        this.timer = timer;
    }

    public void fireKeyPressEvent(int eventCode) {
        switch (eventCode) {
            case KeyEvent.VK_LEFT:
                moveLeft();
                break;
            case KeyEvent.VK_RIGHT:
                moveRight();
                break;
            case KeyEvent.VK_DOWN:
                moveDown();
                break;
            case KeyEvent.VK_SPACE:
                jumpToBottom();
                break;
            case KeyEvent.VK_P:
                pause();
                break;
            case KeyEvent.VK_ESCAPE:
                pause();
                break;
            case KeyEvent.VK_UP:
                rotate();
                break;
            case KeyEvent.VK_ENTER:
                beginGame();
                break;
            case KeyEvent.VK_0:
                board.clearAll();
                break;
            default:
                break;
        }
    }

    public void moveDown() {
        if (!pause) {
            boolean completedMoved = actions.moveDown(currentTetromino.getCurrentTetromino(), board);
            if (completedMoved) {
                jumpToBottom();
            } else {
                gamePanel.repaint();
            }
        }
    }

    private void moveLeft() {
        if (!pause) {
            actions.moveLeft(currentTetromino.getCurrentTetromino(), board);
            gamePanel.repaint();
        }
    }

    private void moveRight() {
        if (!pause) {
            actions.moveRight(currentTetromino.getCurrentTetromino(), board);
            gamePanel.repaint();
        }
    }

    private void jumpToBottom() {
        if (!pause) {
            actions.jumpToBottom(currentTetromino.getCurrentTetromino(), board);
            completeCurrentMove();
            gamePanel.repaint();
        }
    }

    private void completeCurrentMove() {
        if (board.completeRow()) {
            score.updateLabels(board);
            gameEnded = false;
        }
        if (board.gameEnded()) {
            endGame();
        }
        currentTetromino.getCurrentTetromino().resetOrintation();
        currentTetromino.newTetromino();
    }

    private void endGame() {
        if (!gameEnded) {
            pause = true;
            gameEnded = true;
            JOptionPane.showMessageDialog(gamePanel, score.getEndMessage());
        }
    }

    private void pause() {
        if (gameStarted && !gameEnded) {
            pause ^= true;
        }
    }

    private void rotate() {
        if (rotationPainted) {
            currentTetromino.getCurrentTetromino().rotate(board);
        }
        rotationPainted = false;
    }

    private void beginGame() {
        if (!gameEnded && !gameStarted) {
            pause = false;
            gameStarted = true;
            timer.start();
        }
    }

    public void setRotationPainted(boolean value) {
        rotationPainted = value;
    }
}
