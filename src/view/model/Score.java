package view.model;

import board.BoardController;

import javax.swing.*;

public class Score {

    private final int speedUp;
    private final int nextScoreAmount;
    private JLabel scoreLabel;
    private JLabel levelLabel;
    private int score;
    private int timerTime;
    private int nextLevelScore;
    private int level;

    public Score(JLabel scoreLabel, JLabel levelLabel) {
        this.scoreLabel = scoreLabel;
        this.levelLabel = levelLabel;
        timerTime = 500;
        score = 0;
        speedUp = 20;
        nextScoreAmount = 500;
        level = 1;
        nextLevelScore = nextScoreAmount*level;
    }

    public void updateLabels(BoardController board) {
        score = board.vacatRow(score);
        updateLabels();
    }

    private void updateLabels() {
        scoreLabel.setText(getScoreMessage());
        levelLabel.setText(getLevelMessage());
        scoreLabel.repaint();
        levelLabel.repaint();
    }

    public String getScoreMessage() {
        return "Score is " + score;
    }

    public String getLevelMessage() {
        return "Level is " + level;
    }

    public void nextLevel(Timer timer) {
        if (timerTime > 75 && score >= nextLevelScore) {
            level++;
            timerTime -= speedUp;
            timer.setDelay(timerTime);
            nextLevelScore = nextScoreAmount * level;
            nextLevel(timer);
        }
    }

    public String getEndMessage() {
        return "Game Ended.\nGot to level " + level + " with a score of " + score;
    }
}
