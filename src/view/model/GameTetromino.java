package view.model;

import board.BoardController;
import tetromino.Tetromino;
import tetromino.TetrominoBlock;
import tetromino.TetrominoFactory;

import javax.swing.*;

public class GameTetromino {

    private final BoardController board;
    private final TetrominoBlock nextTetrominoStartPosition;
    private final TetrominoFactory factory;
    private Tetromino currentTetromino;
    private Tetromino nextTetromino;
    private final JPanel nextTetrominoPanel;

    public GameTetromino(BoardController board, JPanel nextTetrominoPanel) {
        this.board = board;
        this.nextTetrominoPanel = nextTetrominoPanel;
        nextTetrominoStartPosition = new TetrominoBlock(2, 1, true);
        factory = new TetrominoFactory();
        newNextTetromino();
        newTetromino();
    }

    public void newTetromino() {
        currentTetromino = nextTetromino;
        currentTetromino.setStartPosition(board.getStartPosition());
        newNextTetromino();
    }

    private void newNextTetromino() {
        nextTetromino = factory.getRandomTetromino();
        nextTetromino.setStartPosition(nextTetrominoStartPosition);
        nextTetrominoPanel.repaint();
    }

    public Tetromino getCurrentTetromino() {
        return currentTetromino;
    }

    public Tetromino getNextTetromino() { return nextTetromino; }
}
