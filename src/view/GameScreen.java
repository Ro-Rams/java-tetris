package view;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import board.BoardInvariants;
import board.BoardController;
import view.model.GameTetromino;
import view.model.Score;


@SuppressWarnings("serial")
public class GameScreen extends JFrame implements KeyListener, ActionListener {

	private JPanel contentPane;
	private JPanel gamePanel;
	private JPanel nextTetrominoPanel;
	private JLabel scoreLabel;
	private JLabel levelLabel;
	private Timer timer;
	private BoardController board;
	private Score score;
	private GameEvents events;
	private GameTetromino currentTetromino;

	public static void main(String[] args) {
		EventQueue.invokeLater(() -> {
			try {
				GameScreen frame = new GameScreen();
				frame.setVisible(true);
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}

	public GameScreen() {
		intialiseBoard();
		createSwingComponents();
		currentTetromino = new GameTetromino(board, nextTetrominoPanel);
		createGameUtils();
	}

	private void intialiseBoard() {
		BoardInvariants columnInvariants = new BoardInvariants(10, 5, 15);
		BoardInvariants rowInvariants = new BoardInvariants(27, 0, 15);
		board = new BoardController(columnInvariants, rowInvariants);
	}

	private void createSwingComponents() {
		setTitle("Tetris");
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setBounds(200, 200, 305, 483);
		setResizable(false);
        //setBackground(Color.black);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.setBounds(0, 0, 305, 483);
		//contentPane.setBackground(Color.WHITE);

		gamePanel = new JPanel(new BorderLayout()) {
			@Override
			public void paintComponent(Graphics g) {
				super.paintComponent(g);
				board.draw(g);
				board.drawTetromino(currentTetromino.getCurrentTetromino().getTetrominoPositions(), g, currentTetromino.getCurrentTetromino().getColour());
				g.dispose();
				events.setRotationPainted(true);
			}
		};
		gamePanel.setBounds(15, 10, 165, 420);
		gamePanel.setOpaque(true);
		//gamePanel.setBackground(Color.GRAY);
		contentPane.add(gamePanel);

		nextTetrominoPanel = new JPanel() {
			@Override
			public void paintComponent(Graphics g) {
				super.paintComponent(g);
				board.drawNextTetromino(currentTetromino.getNextTetromino().getTetrominoPositions(), g, currentTetromino.getNextTetromino().getColour());
				g.dispose();
			}
		};
		nextTetrominoPanel.setBounds(215, 10, 80, 70);
		nextTetrominoPanel.setBackground(Color.GRAY);
		contentPane.add(nextTetrominoPanel);


		scoreLabel = new JLabel("Score is 0");
		scoreLabel.setBounds(215, 110, 100, 20);
		contentPane.add(scoreLabel);

		levelLabel = new JLabel("Level is 0");
		levelLabel.setBounds(215, 130, 100, 20);
		contentPane.add(levelLabel);
	}

	private void createGameUtils() {
		timer = new Timer(500, this);
		score = new Score(scoreLabel, levelLabel);
		events = new GameEvents(gamePanel, score, board, timer, currentTetromino);
	}

	public void addNotify() {
		super.addNotify();
		requestFocus();
		addKeyListener(this);
	}
	
	public void keyPressed(KeyEvent event) {
		events.fireKeyPressEvent(event.getKeyCode());
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		events.moveDown();
		score.nextLevel(timer);
	}
	
	@Override
	public void keyReleased(KeyEvent arg0) {
		return;
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		return;
	}
}