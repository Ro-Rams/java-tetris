package view;

import board.model.Coordinate;
import board.model.Board;
import tetromino.TetrominoBlock;

import java.awt.*;

public class GameDrawer {

    private Board board;

    public GameDrawer(Board board) {
        this.board = board;
    }

    public void drawTetromino(TetrominoBlock[] positions, Graphics g, Color colour) {
        for (TetrominoBlock position : positions) {
            Coordinate coord = board.getCoordinateInBoard(position.getRow(), position.getColumn());
            clearPosition(coord, g);
            drawRect(coord, g, colour);
        }
    }

    public void drawNextTetromino(TetrominoBlock[] positions, Graphics g, Color colour) {
        for (TetrominoBlock position : positions) {
            Coordinate coord = new Coordinate(position.getColumn()*board.columnMoveDistance(), position.getRow()*board.rowMoveDistance(), true);
            clearPosition(coord, g);
            drawRect(coord, g, colour);
        }
    }

    private void clearPosition(Coordinate coord, Graphics g) {
        g.clearRect(coord.getColumnCoordinate(), coord.getRowCoordinate(), board.columnMoveDistance(), board.rowMoveDistance());
    }

    private void drawRect(Coordinate coord, Graphics g, Color colour) {
        g.setColor(Color.WHITE);
        g.fillRect(coord.getColumnCoordinate(), coord.getRowCoordinate(), board.columnMoveDistance(), board.rowMoveDistance());
        g.setColor(colour);
        g.drawRect(coord.getColumnCoordinate(), coord.getRowCoordinate(), board.columnMoveDistance(), board.rowMoveDistance());
    }

    public void drawUnoccupiedBlocks(Graphics g) {
        for (Integer key : board.keySet()) {
            for (Coordinate coord : board.get(key)) {
                if (!coord.isOccupied()) {
                    clearPosition(coord, g);
                    drawLines(coord, g);
                }
            }
        }
    }

    private void drawLines(Coordinate coord, Graphics g) {
        g.setColor(coord.getColour());
        g.fillRect(coord.getColumnCoordinate(), coord.getRowCoordinate(), coord.getColumnCoordinate() + board.columnMoveDistance(), coord.getRowCoordinate() + board.rowMoveDistance());
        g.setColor(Color.WHITE);
        g.drawRect(coord.getColumnCoordinate(), coord.getRowCoordinate(), coord.getColumnCoordinate() + board.columnMoveDistance(), coord.getRowCoordinate() + board.rowMoveDistance());
    }

    public void drawOccupiedBlocks(Graphics g) {
        for (Integer key : board.keySet()) {
            for (Coordinate coord : board.get(key)) {
                if (coord.isOccupied()) {
                    drawRect(coord, g, coord.getColour());
                }
            }
        }
    }

    private void drawRect(Coordinate coord, Graphics g) {
        g.setColor(coord.getColour());
        g.drawRect(coord.getColumnCoordinate(), coord.getRowCoordinate(), board.columnMoveDistance(), board.rowMoveDistance());
    }
}
